var express = require('express');
var axios = require('axios');
var router = express.Router();
const url="http://jsonplaceholder.typicode.com/posts"

router.get('/', async function (req, res, next) {
  try {
    const axiosResponse = await axios.get(url)
    res.json(axiosResponse.data);
  } catch (error) {
    next(error)
  }
});

module.exports = router;
