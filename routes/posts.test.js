const posts = require("./posts");

const request = require("supertest");
const express = require("express");
const app = express();

app.use(express.urlencoded({ extended: false }));
app.use("/posts", posts);

test("index route works", done => {
    request(app)
        .get("/posts")
        .expect("Content-Type", /json/)
        .expect(200, done);
});
